import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PasswordTest {
    PasswordGenerator p = new PasswordGenerator(4);
    String passWeak = p.generateWeakPassword();
    String passStrong = p.generateStrongPassword();

    // this test will fail
    @Test
    public void test() {
        fail("this test will fail");
    }

    @Test
    // this test will succeed. In practice we would put
    // a real method call into the assertEquals
    public void testWeak() {
        String pass = "asdf";
        assertEquals(passWeak.length(), pass.length());
    }

    @Test
    public void testStrong() {
        String pass = "asdasdff";
        assertEquals(passStrong.length(), pass.length());
    }
}