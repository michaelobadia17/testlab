public class TestingMethods {
    public static void main (String[] args){
        String word = "Adam's Apple";
        int result = stringCheck(word);
        System.out.print(result);
    }
    public static int stringCheck(String word) {
        int count = 0;
        
        for (int i = 0; i<word.length(); i++) {
            if (word.charAt(i) == 'A' | word.charAt(i) == 'a') {
                count++;
            }
        }
        return count;
    }
}
