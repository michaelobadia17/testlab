public class Login {
    User[] uArr = new User[3];
    public Login(User[] uArr) {
        this.uArr = uArr;
    }

    public boolean validate(String username, String password, boolean admin) {
        for(int i = 0; i < uArr.length; i++) {
            if(username == uArr[i].getUsername() && password == uArr[i].getPassword()) {
                if(admin == false) {
                return true;  
            }
            else {
                if(uArr[i].getUserType() == UserType.ADMIN) {
                    return true;
                }
            }
        }
    }
        return false;
    }
}