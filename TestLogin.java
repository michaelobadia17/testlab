import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestLogin {
    
    @Test
    public void testValidate(){
        User[] arr = setUserArray();
        Login log = new Login(arr);
        assertEquals(false,log.validate(arr[0].getUsername(), arr[0].getPassword(), true));
    }
    @Test
    public void testValidate2(){
        User[] arr = setUserArray();
        Login log = new Login(arr); 
        assertEquals(true,log.validate(arr[2].getUsername(), arr[2].getPassword(), true));
    }
    public User[] setUserArray(){
        User[] arr = new User[3];
        arr[0] = new User("johnny","jkui6753", UserType.REGULAR);
        arr[1] = new User("micheal","mkloij23323", UserType.ADMIN);
        arr[2] = new User("jack","hdsjhdsjhd2567", UserType.ADMIN);
        return arr;
    } 
}
