public class User {
    private String username;
    private String password;
    private UserType userT;

    public User(String username, String password, UserType userT){
        this.username = username;
        this.password = password;
        this.userT = userT;
    }
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public UserType getUserType(){
        return this.userT;
    }
}


