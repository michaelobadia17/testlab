import java.util.Random;
public class PasswordGenerator { 
    private int length;
    private Random randGen = new Random();
    
    public PasswordGenerator(int length) {
        this.length = length;
    }

    public String generateWeakPassword(){
        String weak = "abcdefghijklmnopqrstuvwxyz";
        String password = "";
        final int ALPHABET_NUMBER = 26;
        for (int i = 0; i<this.length; i++){
            password += weak.charAt(randGen.nextInt(ALPHABET_NUMBER));
        }
        return password;
    }

    public String generateStrongPassword(){
        String strong = "aAbBcCdeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";
        String password = "";
        final int WHOLE_ALPHABET = 52;
        for (int i = 0; i<this.length*2; i++){
            password += strong.charAt(randGen.nextInt(WHOLE_ALPHABET));
        }
        return password;

    }
}